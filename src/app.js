// @flow

console.log('Oppgave 1');
{
  let v1 = [1, 2, 3];
  let v2 = [4, 5, 6];

  console.log('2 + v1:', v1.map(e => 2 + e));

  console.log('2 * v1:', v1.map(e => 2 * e));

  console.log('mean of v1:', v1.reduce((acc, e) => acc + e) / v1.length);

  console.log('v1 dot v2:', v1.reduce((acc, e, i) => acc + e * v2[i], 0));

  console.log('sum of v1 + 2 * v2:', v1.map((e, i) => e + 2 * v2[i]).reduce((acc, e) => acc + e));

  console.log(
    'v1 as string:',
    v1.map((e, i) => 'v1[' + i.toString() + '] = ' + e).reduce((acc, e) => acc + (acc == '' ? '' : ', ') + e)
  );
}

console.log('Oppgave 2');
{
  class Complex {
    real: number;
    imag: number;

    constructor(real: number, img: number) {
      this.real = real;
      this.imag = img;
    }
  }

  let v = [new Complex(2, 2), new Complex(1, 1)];

  console.log('v elements as strings:', v.map(e => e.real.toString() + ' + ' + e.imag.toString() + 'i'));

  console.log('magnitude of v elements:', v.map(e => Math.sqrt(Math.pow(e.real, 2) + Math.pow(e.imag, 2))));

  console.log(
    'sum of v:',
    v.reduce((acc, e) => {
      acc.real += e.real;
      acc.imag += e.imag;
      return acc;
    })
  );
}

console.log('Oppgave 3');
{
  let students = [{ name: 'Ola', grade: 'A' }, { name: 'Kari', grade: 'C' }, { name: 'Knut', grade: 'C' }];

  console.log('students elements as strings:', students.map(e => e.name + ' got ' + e.grade));

  console.log('How many got C:', students.filter(e => e.grade == 'C').length);

  console.log('Percentage of C grades:', students.filter(e => e.grade == 'C').length / students.length);

  console.log('Did anyone get A:', students.some(e => e.grade == 'A') ? 'Yes' : 'No');
  console.log('Did anyone get F:', students.some(e => e.grade == 'F') ? 'Yes' : 'No');
}
